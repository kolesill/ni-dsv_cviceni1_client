import java.rmi.registry.*;
import java.util.Scanner;

public class HelloClient {
	
	private static String xor(String msg) {
		byte key[] = "2384ytgbhbgsvduig8712t3fiquwgbibkqwjfbihoubfvkjn".getBytes();
		byte msg_bytes[] = msg.getBytes();
		for (int i = 0; i < msg_bytes.length; ++i) {
			int key_idx = i % key.length;
			msg_bytes[i] ^= key[key_idx];
		}
		return new String(msg_bytes); 
	}

	public static void main(String args[])
	{
		try
		{
		// specify local or remote ip
		String ip = "127.0.0.1";
		int port = 10900;
		Registry reg = LocateRegistry.getRegistry(ip, port);
		// stub creation
		Hello stub = (Hello)reg.lookup("Hello");
		
		System.out.println("Chat started, send massage:");
		while (true) {
			Scanner scan = new Scanner(System.in);
			String msg_sent = xor(scan.nextLine());
			System.out.println("Encoded message sent: " + msg_sent);
			String msg_recv = xor(stub.chat(msg_sent));
			System.out.println("Server> " + msg_recv);
		}
		}
		
		catch (Exception e)
		{
			System.out.println("Err: "+e.toString());
		}
		
	}

}
