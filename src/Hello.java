import java.rmi.*;

public interface Hello extends Remote {
	
	String chat(String msg) throws RemoteException;
}
